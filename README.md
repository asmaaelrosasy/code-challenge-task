# Code Challenge Task	

a simple dictionary key/value store script using only core NodeAPI and ECMAScript 5 or 6.

## Prerequisites

Node 

Docker

### Running


Store Commands :

$ node store.js add mykey myvalue  or ./store.js add my key myvalue

$ node store.js list		       or ./store.js list

$ node store.js get mykey          or ./store.js get mykey

$ node store.js remove mykey       or ./store.js remove mykey

$ node store.js clear              or ./store.js clear

_____________________________________________________

Commands to deploy and run it in docker container : 

$ docker build -t my-node-app .

$ docker run my-node-app <command>

note : fs.writeFile may doesn't work on Docker , most probably due to permissions.






