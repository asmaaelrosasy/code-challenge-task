FROM node
RUN mkdir /usr/src/app
WORKDIR /usr/src/app/
COPY . package.json /usr/src/app/
ENTRYPOINT ["node", "store.js"]